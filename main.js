$(document).ready(function(){
    $("head").pinify({
        favIcon: "../img/favicon.ico",
        tooltip: "Se statistiken för kösystemet"
    });
    window.setTimeout(function(){
        $.pinify.addOverlay({
            title: "Example Notification",
            icon: "../img/1.ico"
        });
        $.pinify.flashTaskbar();
    }, 2000);
});