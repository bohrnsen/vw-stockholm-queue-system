﻿var count;

function initApp() {
    count = 1;
    clearOverlayIcon();
    window.setTimeout(function () {
        setOverlayIcon();
    }, 2000);
    createCustomList("Kösystem");
    addJumpListEntry("("+count+") Reservdelar", "http://google.de");
}

function clearOverlayIcon() {
    try {
        if (window.external.msIsSiteMode()) {
            window.external.msSiteModeClearIconOverlay();
        }
    }
    catch (e) {
        console.log("Failed to clear overlay");
    }
}

function setOverlayIcon() {
    try {
        if (window.external.msIsSiteMode()) {
            window.external.msSiteModeSetIconOverlay("http://kai-bohrnsen.com/DinBil/Kösystem/icons/"+count+".ico", count);
        }
    }
    catch (e) {
        console.log("Failed to set overlay");
    }
}

function createCustomList(name) {
    try {
        if (window.external.msIsSiteMode()) {
            window.external.msSiteModeCreateJumplist(name);
        }
    }
    catch (e) {
        console.log("Failed to create custom list");
    }
}

function addJumpListEntry(searchValue, destination) {
    window.external.msSiteModeAddJumpListItem(searchValue, destination, "http://kai-bohrnsen.com/DinBil/Kösystem/favicon.ico");
    window.external.msSiteModeShowJumplist();
}